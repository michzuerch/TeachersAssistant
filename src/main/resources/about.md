# Verwaltungsprogramm für Lehrer

Vorbild ist eine kommerzielle Software.

Datenbank Postgresql.

Verwaltung der Stammdaten, Schule, Lehrer, Klasse, Schüler.

Stundenpläne mit Schnittstelle zu Google Kalender.

Zeugnisse drucken, Formulare mit Jasperreport und XML-FO.

Geplant ist eine Umstellung auf Mongo DB und die Integration von Keycloak.

Sourcecode
[from github](https://github.com/michzuerch/LehrerVerwaltung).

