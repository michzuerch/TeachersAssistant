package com.gmail.michzuerch.TeachersAssistant.backend.jpa.repository;

import com.gmail.michzuerch.TeachersAssistant.backend.jpa.domain.report.jasper.ReportJasper;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportJasperRepository extends JpaRepository<ReportJasper, Long> {
}
