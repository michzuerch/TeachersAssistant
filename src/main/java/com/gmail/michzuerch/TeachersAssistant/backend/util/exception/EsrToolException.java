package com.gmail.michzuerch.TeachersAssistant.backend.util.exception;

public class EsrToolException extends Exception {
    /**
	 *
	 */
	private static final long serialVersionUID = -5483594615800758924L;

	public EsrToolException() {
    }

    public EsrToolException(String message) {
        super(message);
    }
}
