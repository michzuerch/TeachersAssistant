package com.gmail.michzuerch.TeachersAssistant.backend.jpa.repository;

import com.gmail.michzuerch.TeachersAssistant.backend.jpa.domain.report.fop.ReportFOPImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportFOPImageRepository extends JpaRepository<ReportFOPImage, Long> {
}
