package com.gmail.michzuerch.TeachersAssistant.backend.jpa.repository;

import com.gmail.michzuerch.TeachersAssistant.backend.jpa.domain.report.jasper.ReportJasperImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportJasperImageRepository extends JpaRepository<ReportJasperImage, Long> {
}
