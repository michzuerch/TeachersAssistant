package com.gmail.michzuerch.TeachersAssistant.backend.jpa.repository;

import com.gmail.michzuerch.TeachersAssistant.backend.jpa.domain.report.fop.ReportFOP;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportFOPRepository extends JpaRepository<ReportFOP, Long> {
}
