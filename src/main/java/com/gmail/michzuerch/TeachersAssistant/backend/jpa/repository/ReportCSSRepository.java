package com.gmail.michzuerch.TeachersAssistant.backend.jpa.repository;

import com.gmail.michzuerch.TeachersAssistant.backend.jpa.domain.report.css.ReportCSS;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportCSSRepository extends JpaRepository<ReportCSS, Long> {
}
