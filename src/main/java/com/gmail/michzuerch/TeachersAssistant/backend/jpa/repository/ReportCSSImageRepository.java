package com.gmail.michzuerch.TeachersAssistant.backend.jpa.repository;

import com.gmail.michzuerch.TeachersAssistant.backend.jpa.domain.report.css.ReportCSSImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportCSSImageRepository extends JpaRepository<ReportCSSImage, Long> {
}
