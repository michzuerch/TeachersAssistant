TeachersAssistant
==============

Verwalten von Schulungsräumen, Lehrern, Schülern, Stundenplänen.

Funktionsumfang
========

Prototyp mit provisorischem Datenmodell.


Technologien
-------------------------

- Wildfly 11.0
- Vaadin 8.3.1
- Postgresql
- FOP
- Jasperreport


Installation
-------------------------

Source ist auf github: https://github.com/michzuerch/TeachersAssistant.git

Das Buildsystem ist Maven. Zum Installieren mvn "wildfly:deploy" eingeben.

Um Unterordner "Scripts" sind die Shell-Scripts zum erstellen der Datasources.
